<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Credential;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends AbstractController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return 'ok';
    }

    public function saveCredential(Request $request)
    {
        $data = $request->all();
        $cred = new Credential();
        $cred->fill($data);
        $cred->save();

        return response()->json([
            'status' => 'ok',
        ]);
    }

    public function editCredential(int $id, Request $request)
    {
        $cred = Credential::find($id);

        if (empty($cred)) {
            return response()->json([
                'status' => 'error',
                'error' => 'Cant find user with id ' . $id,
            ]);
        }
        $data = $request->all();
        $cred->fill($data);
        $cred->save();

        return response()->json([
            'status' => 'ok',
        ]);
    }

    public function deleteCredential(int $id)
    {
        $cred = Credential::find($id);

        if (empty($cred)) {
            return response()->json([
                'status' => 'error',
                'error' => 'Cant find user with id ' . $id,
            ]);
        }
        $cred->delete();

        return response()->json([
            'status' => 'ok',
        ]);
    }

    public function getCredential(int $id)
    {
        $cred = Credential::find($id);

        if (empty($cred)) {
            return response()->json([
                'status' => 'error',
                'error' => 'Cant find user with id ' . $id,
            ]);
        }


        return response()->json([
            'status' => 'ok',
            'data' => $cred->toArray(),
        ]);
    }

    public function getAllCredential()
    {
        $cred = Credential::all();
        return response()->json([
            'status' => 'ok',
            'data' => $cred->toArray(),
        ]);
    }

}
