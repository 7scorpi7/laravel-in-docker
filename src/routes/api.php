<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('user', ['uses' => 'IndexController@saveCredential']);
Route::patch('user/{id}', ['uses' => 'IndexController@editCredential'])->where('id', '[0-9]+');
Route::get('user/{id}', ['uses' => 'IndexController@getCredential'])->where('id', '[0-9]+');
Route::delete('user/{id}', ['uses' => 'IndexController@deletegiCredential'])->where('id', '[0-9]+');
Route::get('user/', ['uses' => 'IndexController@getAllCredential']);

